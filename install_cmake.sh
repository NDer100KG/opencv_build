wget https://cmake.org/files/v3.13/cmake-3.13.0-Linux-x86_64.tar.gz
tar -xzvf cmake-3.13.0-Linux-x86_64.tar.gz
rm -rf cmake-3.13.0-Linux-x86_64.tar.gz

mv cmake-3.13.0-Linux-x86_64 /opt/cmake-3.13.0

ln -sf /opt/cmake-3.13.0/bin/*  /usr/bin/

cmake --version
