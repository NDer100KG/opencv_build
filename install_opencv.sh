######################################
# INSTALL OPENCV ON UBUNTU OR DEBIAN #
######################################

# -------------------------------------------------------------------- |
#                       SCRIPT OPTIONS                                 |
# ---------------------------------------------------------------------|
OPENCV_VERSION='3.4.2'       # Version to be installed
BUILD_ANDROID="NO"
ANDROID_NDK_PATH="/opencv_build/Android/android-ndk-r17c"
ANDROID_SDK_PATH="/opencv_build/Android"
# -------------------------------------------------------------------- |

if [ $# = 2 ]; then
  OPENCV_VERSION=$1
  BUILD_ANDROID=$2
fi

echo $1
echo "OPENCV_VERSION: " ${OPENCV_VERSION}
echo "BUILD_ANDROID:  " ${BUILD_ANDROID}
# exit 1

if [ ! -d opencv-${OPENCV_VERSION} ];
then
  wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip
  unzip ${OPENCV_VERSION}.zip && rm ${OPENCV_VERSION}.zip

  wget https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip
  unzip ${OPENCV_VERSION}.zip && rm ${OPENCV_VERSION}.zip
  mv opencv_contrib-${OPENCV_VERSION} opencv-${OPENCV_VERSION}
fi

cd opencv-${OPENCV_VERSION}

if [ $BUILD_ANDROID = 'NO' ]; then

  mkdir -p build/x86 && cd build/x86
  cmake -DWITH_QT=ON \
        -DWITH_OPENGL=ON \
        -DFORCE_VTK=ON \
        -DWITH_TBB=ON \
        -DWITH_GDAL=ON \
        -DWITH_XINE=ON \
        -DWITH_CUDA=OFF \
        -DBUILD_DOCS=OFF \
        -DBUILD_TESTS=ON \
        -DWITH_MATLAB=OFF \
        -DENABLE_PRECOMPILED_HEADERS=OFF \
        -DBUILD_TIFF=ON \
        -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-${OPENCV_VERSION}/modules \
        ../..

fi

if [ $BUILD_ANDROID = 'YES' ]; then
  export ANDROID_SDK_ROOT=${ANDROID_SDK_PATH}
  ANDROID_ABI_LIST="arm64-v8a"

  echo "Start building ${ANDROID_ABI} version"

  mkdir -p build/${ANDROID_ABI} && cd build/${ANDROID_ABI}

  cmake -DCMAKE_TOOLCHAIN_FILE=${ANDROID_NDK_PATH}/build/cmake/android.toolchain.cmake \
    -DANDROID_NDK=${ANDROID_NDK_PATH} \
    -DANDROID_NATIVE_API_LEVEL=23 \
    -DANDROID_ABI="${ANDROID_ABI}" \
    -D WITH_CUDA=OFF \
    -D WITH_MATLAB=OFF \
    -D BUILD_ANDROID_EXAMPLES=OFF \
    -D BUILD_DOCS=OFF \
    -D BUILD_TESTS=ON \
    -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-${OPENCV_VERSION}/modules \
    ../..

  echo "End building ${ANDROID_ABI} version"
fi

make -j24
make install
# sudo ldconfig
