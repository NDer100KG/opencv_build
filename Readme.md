# OPENCV_BUILD

[![pipeline status](https://gitlab.com/NDer100KG/opencv_build/badges/master/pipeline.svg)](https://gitlab.com/NDer100KG/opencv_build/-/commits/master)

## Build opencv
1. dependencies:
   ```
   ./install_dependencies.sh

   ## if building android > r17c, cmake > 3.6 is needed
   ## if building android, you should download corresponding android ndk and android sdk in advance
   ./install_cmake.sh
   ```
2. scripts:
   * The command be like `./install_opencv.sh ${OPENCV_VERSION} {BUILD ANDROID}`, for example:
      ```
      ./install_opencv.sh 3.4.2 YES
      ```
   * After building, you can run unittests in bin folder, it shows whether your build success
      ```
      ./opencv-3.4.2/build/x86/bin/opencv_test_core
      ```

## Building gitlab runner
1. Install the Docker image and start the container
   ```
    docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
   ```

2. Register the runner
   ```
   docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
   ```
   * Enter your GitLab instance URL (also known as the gitlab-ci coordinator URL).
   * Enter the token you obtained to register the runner.
   * Enter a description for the runner. You can change this value later in the GitLab user interface.
   * Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface.
   * Provide the runner executor. For most use cases, enter docker.
   * If you entered docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in `.gitlab-ci.yml`.

3. You should see Available specific runners in your CI/CD settings page
   
    <img src="pic/gitlab_runner.png" alt="drawing">

4. Note
   * if encounter "undefined reference to `TIFFClose@LIBTIFF_4.0" 
     * add `-DBUILD_TIFF=ON`
     * `conda uninstall libtiff`
   * In docker, no need to use `sudo`

## References

1. [opencv_install.sh](https://github.com/milq/milq/blob/master/scripts/bash/install-opencv.sh)
2. [opencv build for android](https://github.com/tzutalin/build-opencv-for-android/blob/master/README.md?fbclid=IwAR39sOa_VZ9aE52iZJU3I_a_2a5GAuEWJf6ptUTGC0oM_0-HcOiz9u6Ck-k)
3. [如何使用 GitLab CI](https://medium.com/@mvpdw06/%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8-gitlab-ci-ebf0b68ce24b) 
4. [Gitlab CI doc](https://docs.gitlab.com/ee/ci/yaml/)
5. [Gitlab pipeline architecture](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html)
